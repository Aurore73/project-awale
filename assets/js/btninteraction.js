import {whichplayerbegin} from "./global.js"
import {recupgrains} from "./recupgrains.js"

//count number of seeds
export function nbseed() {

    //Listing of all li tag in the html page of the awale game
    let listgrains = document.querySelectorAll("li")

    return listgrains

}

//add seed on granaries
function addseed(grains,id_player){

    if(id_player === grains.className){

        let idseed = parseInt(grains.id)
        let nbseeds = parseInt(grains.innerHTML)
        
        // adding grains in each granaries 
        while(nbseeds > 0) {

            idseed++

            idseed = (idseed == parseInt(grains.id)) ? idseeds++ : idseed

            idseed = (idseed == 13) ? 1 : idseed

            let idsenext = idseed.toString()
            let senext = grains.parentElement.querySelector(`#${CSS.escape(idsenext)}`)
            senext.innerHTML = parseInt(senext.innerHTML) + 1 
            
            nbseeds--

       }
    
        grains.innerHTML = 0
        recupgrains(id_player,idseed)

        let next_player = (id_player == "player1") ? "player2" : "player1"

        whichplayerbegin(next_player)
        seeddistribution(next_player)
    }
}

//distrubution of seed by the player
export function seeddistribution(player){

    let listseeds = nbseed()
    //Browsing of all li tag in the html page of the awwale game
    listseeds.forEach(seeds => {

             //Adding event listener for each li tag when the player click on the granary
            seeds.addEventListener("click",function(event){

            addseed(seeds,player)

            })
       
    })

}
