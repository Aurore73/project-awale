/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/btninteraction.js":
/*!*************************************!*\
  !*** ./assets/js/btninteraction.js ***!
  \*************************************/
/*! exports provided: nbseed, seeddistribution */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"nbseed\", function() { return nbseed; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"seeddistribution\", function() { return seeddistribution; });\n/* harmony import */ var _global_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./global.js */ \"./assets/js/global.js\");\n/* harmony import */ var _recupgrains_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./recupgrains.js */ \"./assets/js/recupgrains.js\");\n\n\n\n//count number of seeds\nfunction nbseed() {\n\n    //Listing of all li tag in the html page of the awale game\n    let listgrains = document.querySelectorAll(\"li\")\n\n    return listgrains\n\n}\n\n//add seed on granaries\nfunction addseed(grains,id_player){\n\n    if(id_player === grains.className){\n\n        let idseed = parseInt(grains.id)\n        let nbseeds = parseInt(grains.innerHTML)\n        \n        // adding grains in each granaries \n        while(nbseeds > 0) {\n\n            idseed++\n\n            idseed = (idseed == parseInt(grains.id)) ? idseeds++ : idseed\n\n            idseed = (idseed == 13) ? 1 : idseed\n\n            let idsenext = idseed.toString()\n            let senext = grains.parentElement.querySelector(`#${CSS.escape(idsenext)}`)\n            senext.innerHTML = parseInt(senext.innerHTML) + 1 \n            \n            nbseeds--\n\n       }\n    \n        grains.innerHTML = 0\n        Object(_recupgrains_js__WEBPACK_IMPORTED_MODULE_1__[\"recupgrains\"])(id_player,idseed)\n\n        let next_player = (id_player == \"player1\") ? \"player2\" : \"player1\"\n\n        Object(_global_js__WEBPACK_IMPORTED_MODULE_0__[\"whichplayerbegin\"])(next_player)\n        seeddistribution(next_player)\n    }\n}\n\n//distrubution of seed by the player\nfunction seeddistribution(player){\n\n    let listseeds = nbseed()\n    //Browsing of all li tag in the html page of the awwale game\n    listseeds.forEach(seeds => {\n\n             //Adding event listener for each li tag when the player click on the granary\n            seeds.addEventListener(\"click\",function(event){\n\n            addseed(seeds,player)\n\n            })\n       \n    })\n\n}\n\n\n//# sourceURL=webpack:///./assets/js/btninteraction.js?");

/***/ }),

/***/ "./assets/js/global.js":
/*!*****************************!*\
  !*** ./assets/js/global.js ***!
  \*****************************/
/*! exports provided: whichplayerbegin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"whichplayerbegin\", function() { return whichplayerbegin; });\n/* harmony import */ var _btninteraction_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./btninteraction.js */ \"./assets/js/btninteraction.js\");\n\n\n// array player\nlet random = [\"player1\",\"player2\"]\n\n//randomly generator of a number to choose a player\nfunction getRandomInt(max) {\n    return Math.floor(Math.random() * Math.floor(max));\n}\n\n//modify style if it's player turn linked to the function swithplayer\nfunction modifystyle(id,col){\n\n    let listliplayer = document.getElementsByClassName(id)\n\n        for (let index = 0; index < listliplayer.length; index++) {\n            \n            if(col == \"red\"){\n\n                listliplayer.item(index).style.color = col\n                listliplayer.item(index).style.cursor = \"pointer\" \n\n            }else if(col == \"blue\"){\n\n                listliplayer.item(index).style.color = col\n                listliplayer.item(index).style.cursor = \"pointer\" \n\n            }else if(col == \"grey\"){\n\n                listliplayer.item(index).style.color = col\n                listliplayer.item(index).style.cursor = \"not-allowed\"  \n            }\n            \n        }\n\n}\n\n//disable player's granaries when it is not his turn linked to the function swithplayer\nfunction disabledplayer(id){\n\n    let listliplayerop = document.getElementsByClassName(id)\n\n    for (let index = 0; index < listliplayerop.length; index++) {\n            \n        listliplayerop.item(index).removeEventListener(\"click\",function(e){\n            e.stopPropagation()\n        },true)\n            \n    }\n\n}\n\n// read my table (for player 1 or player 2) from begin to end and define border color \n//and color for each player and to prohibit click on the other side \nfunction switchplayer(player_id,color){\n\n    if(player_id == \"player1\"){\n\n        modifystyle(player_id,color)\n        \n        disabledplayer(\"player2\")\n\n    }else{\n\n        modifystyle(player_id,color)\n\n        disabledplayer(\"player1\")\n\n    }\n   \n}\n\n// 2 cases : the player1 begins so color of the player1 game side change into red;\n//           and the message changed and the player is indicated.\n//           the player2 beings so color changes into blue.\nfunction whichplayerbegin(player_id){\n\n    switch (player_id) {\n        case \"player1\":\n            document.getElementById(player_id).style.color = \"red\"\n            switchplayer(player_id,\"red\")\n            document.getElementById(\"textstart\").innerHTML = \"It's your turn,Player 1\"\n            document.getElementById(\"player2\").style.color = \"grey\"\n            switchplayer(\"player2\",\"grey\")\n            break\n    \n        case \"player2\":\n            document.getElementById(player_id).style.color = \"blue\" \n            switchplayer(player_id,\"blue\")\n            document.getElementById(\"textstart\").innerHTML = \"It's your turn,Player 2\"\n            document.getElementById(\"player1\").style.color = \"grey\"\n            switchplayer(\"player1\",\"grey\")\n            break\n    }\n\n}\n\n//choose which player begin first\nfunction choosewhobegin(tabrandom){\n\n    let result = tabrandom[getRandomInt(2)]\n    \n    whichplayerbegin(result)\n    \n    return result\n}\n\ndocument.addEventListener(\"DOMContentLoaded\",function(){  \n    \n    let player = choosewhobegin(random)\n\n    Object(_btninteraction_js__WEBPACK_IMPORTED_MODULE_0__[\"seeddistribution\"])(player)\n    \n},true)\n\n//# sourceURL=webpack:///./assets/js/global.js?");

/***/ }),

/***/ "./assets/js/recupgrains.js":
/*!**********************************!*\
  !*** ./assets/js/recupgrains.js ***!
  \**********************************/
/*! exports provided: recupgrains */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"recupgrains\", function() { return recupgrains; });\nfunction scoring(id,grains,player,score,lasthole)\n{\n    let ids = (player==\"player1\") ? 6 : 0\n    let arrscore = [0,0]\n    let index = (player==\"player1\") ? 0 : 1\n\n    if(grains.className != player){\n\n        while(id != ids){\n\n            let grainsrecup = grains.parentElement.querySelector(`#${CSS.escape(id.toString())}`)\n            let nbrgrains = parseInt(grainsrecup.innerHTML)\n            if(grainsrecup.id == lasthole){\n    \n                if ((nbrgrains == 2 || nbrgrains == 3) && grainsrecup.className != player ){\n                    score += nbrgrains\n                    grainsrecup.innerHTML = 0\n                    arrscore[index] += score\n                    alert(\"score \"+player+\" : \"+arrscore[index])\n                }\n    \n            }\n            \n            id--\n        }\n    }\n\n    document.getElementById(player+\"-score\").innerHTML = parseInt(document.getElementById(player+\"-score\").innerHTML) + arrscore[index]\n\n}\n\nfunction recupgrains(player, lastHole){\n\n    //if the lastHole is in the adverse side, my functin begins : \n    let grains = document.getElementById(lastHole)\n    let id,score = 0\n\n    id = parseInt(grains.id)\n\n    scoring(id,grains,player,score,lastHole)\n\n}\n\n\n//# sourceURL=webpack:///./assets/js/recupgrains.js?");

/***/ }),

/***/ "./assets/stylesheets/global.css":
/*!***************************************!*\
  !*** ./assets/stylesheets/global.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!./global.css */ \"./node_modules/css-loader/dist/cjs.js!./assets/stylesheets/global.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\nvar exported = content.locals ? content.locals : {};\n\n\n\nmodule.exports = exported;\n\n//# sourceURL=webpack:///./assets/stylesheets/global.css?");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./assets/stylesheets/global.css":
/*!*****************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./assets/stylesheets/global.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"/*Style for awale structure*/\\nul{\\n\\n    list-style: none;\\n    display: grid;\\n    grid-template-columns: repeat(6,1fr);\\n    border-style: solid;\\n    border-radius: 2rem;\\n    width: 80vw;\\n    height: 50vh;\\n    margin: auto;\\n    padding: auto;\\n}\\n\\nul li{\\n    text-align: center;\\n    border-style: solid;\\n    border-radius: 8rem 8rem 8rem 8rem;\\n    padding-bottom: 2rem;\\n    padding-top: 2rem;\\n    margin: 2rem;\\n}\\n\\nul li:hover{\\n    cursor: pointer;\\n}\\n\\n/************************************************/\\n\\n/*Style for beginning introduction text to begin awale game*/\\np{\\n    text-align: center;\\n}\\n\\na{\\n    text-decoration: underline;    \\n}\\n\\na:hover{\\n    cursor: pointer;\\n}\\n\\n/************************************************/\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./assets/stylesheets/global.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\n// css base code, injected by the css-loader\n// eslint-disable-next-line func-names\nmodule.exports = function (useSourceMap) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = cssWithMappingToString(item, useSourceMap);\n\n      if (item[2]) {\n        return \"@media \".concat(item[2], \" {\").concat(content, \"}\");\n      }\n\n      return content;\n    }).join('');\n  }; // import a list of modules into the list\n  // eslint-disable-next-line func-names\n\n\n  list.i = function (modules, mediaQuery, dedupe) {\n    if (typeof modules === 'string') {\n      // eslint-disable-next-line no-param-reassign\n      modules = [[null, modules, '']];\n    }\n\n    var alreadyImportedModules = {};\n\n    if (dedupe) {\n      for (var i = 0; i < this.length; i++) {\n        // eslint-disable-next-line prefer-destructuring\n        var id = this[i][0];\n\n        if (id != null) {\n          alreadyImportedModules[id] = true;\n        }\n      }\n    }\n\n    for (var _i = 0; _i < modules.length; _i++) {\n      var item = [].concat(modules[_i]);\n\n      if (dedupe && alreadyImportedModules[item[0]]) {\n        // eslint-disable-next-line no-continue\n        continue;\n      }\n\n      if (mediaQuery) {\n        if (!item[2]) {\n          item[2] = mediaQuery;\n        } else {\n          item[2] = \"\".concat(mediaQuery, \" and \").concat(item[2]);\n        }\n      }\n\n      list.push(item);\n    }\n  };\n\n  return list;\n};\n\nfunction cssWithMappingToString(item, useSourceMap) {\n  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring\n\n  var cssMapping = item[3];\n\n  if (!cssMapping) {\n    return content;\n  }\n\n  if (useSourceMap && typeof btoa === 'function') {\n    var sourceMapping = toComment(cssMapping);\n    var sourceURLs = cssMapping.sources.map(function (source) {\n      return \"/*# sourceURL=\".concat(cssMapping.sourceRoot || '').concat(source, \" */\");\n    });\n    return [content].concat(sourceURLs).concat([sourceMapping]).join('\\n');\n  }\n\n  return [content].join('\\n');\n} // Adapted from convert-source-map (MIT)\n\n\nfunction toComment(sourceMap) {\n  // eslint-disable-next-line no-undef\n  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));\n  var data = \"sourceMappingURL=data:application/json;charset=utf-8;base64,\".concat(base64);\n  return \"/*# \".concat(data, \" */\");\n}\n\n//# sourceURL=webpack:///./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar isOldIE = function isOldIE() {\n  var memo;\n  return function memorize() {\n    if (typeof memo === 'undefined') {\n      // Test for IE <= 9 as proposed by Browserhacks\n      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805\n      // Tests for existence of standard globals is to allow style-loader\n      // to operate correctly into non-standard environments\n      // @see https://github.com/webpack-contrib/style-loader/issues/177\n      memo = Boolean(window && document && document.all && !window.atob);\n    }\n\n    return memo;\n  };\n}();\n\nvar getTarget = function getTarget() {\n  var memo = {};\n  return function memorize(target) {\n    if (typeof memo[target] === 'undefined') {\n      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself\n\n      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n        try {\n          // This will throw an exception if access to iframe is blocked\n          // due to cross-origin restrictions\n          styleTarget = styleTarget.contentDocument.head;\n        } catch (e) {\n          // istanbul ignore next\n          styleTarget = null;\n        }\n      }\n\n      memo[target] = styleTarget;\n    }\n\n    return memo[target];\n  };\n}();\n\nvar stylesInDom = [];\n\nfunction getIndexByIdentifier(identifier) {\n  var result = -1;\n\n  for (var i = 0; i < stylesInDom.length; i++) {\n    if (stylesInDom[i].identifier === identifier) {\n      result = i;\n      break;\n    }\n  }\n\n  return result;\n}\n\nfunction modulesToDom(list, options) {\n  var idCountMap = {};\n  var identifiers = [];\n\n  for (var i = 0; i < list.length; i++) {\n    var item = list[i];\n    var id = options.base ? item[0] + options.base : item[0];\n    var count = idCountMap[id] || 0;\n    var identifier = \"\".concat(id, \" \").concat(count);\n    idCountMap[id] = count + 1;\n    var index = getIndexByIdentifier(identifier);\n    var obj = {\n      css: item[1],\n      media: item[2],\n      sourceMap: item[3]\n    };\n\n    if (index !== -1) {\n      stylesInDom[index].references++;\n      stylesInDom[index].updater(obj);\n    } else {\n      stylesInDom.push({\n        identifier: identifier,\n        updater: addStyle(obj, options),\n        references: 1\n      });\n    }\n\n    identifiers.push(identifier);\n  }\n\n  return identifiers;\n}\n\nfunction insertStyleElement(options) {\n  var style = document.createElement('style');\n  var attributes = options.attributes || {};\n\n  if (typeof attributes.nonce === 'undefined') {\n    var nonce =  true ? __webpack_require__.nc : undefined;\n\n    if (nonce) {\n      attributes.nonce = nonce;\n    }\n  }\n\n  Object.keys(attributes).forEach(function (key) {\n    style.setAttribute(key, attributes[key]);\n  });\n\n  if (typeof options.insert === 'function') {\n    options.insert(style);\n  } else {\n    var target = getTarget(options.insert || 'head');\n\n    if (!target) {\n      throw new Error(\"Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.\");\n    }\n\n    target.appendChild(style);\n  }\n\n  return style;\n}\n\nfunction removeStyleElement(style) {\n  // istanbul ignore if\n  if (style.parentNode === null) {\n    return false;\n  }\n\n  style.parentNode.removeChild(style);\n}\n/* istanbul ignore next  */\n\n\nvar replaceText = function replaceText() {\n  var textStore = [];\n  return function replace(index, replacement) {\n    textStore[index] = replacement;\n    return textStore.filter(Boolean).join('\\n');\n  };\n}();\n\nfunction applyToSingletonTag(style, index, remove, obj) {\n  var css = remove ? '' : obj.media ? \"@media \".concat(obj.media, \" {\").concat(obj.css, \"}\") : obj.css; // For old IE\n\n  /* istanbul ignore if  */\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = replaceText(index, css);\n  } else {\n    var cssNode = document.createTextNode(css);\n    var childNodes = style.childNodes;\n\n    if (childNodes[index]) {\n      style.removeChild(childNodes[index]);\n    }\n\n    if (childNodes.length) {\n      style.insertBefore(cssNode, childNodes[index]);\n    } else {\n      style.appendChild(cssNode);\n    }\n  }\n}\n\nfunction applyToTag(style, options, obj) {\n  var css = obj.css;\n  var media = obj.media;\n  var sourceMap = obj.sourceMap;\n\n  if (media) {\n    style.setAttribute('media', media);\n  } else {\n    style.removeAttribute('media');\n  }\n\n  if (sourceMap && btoa) {\n    css += \"\\n/*# sourceMappingURL=data:application/json;base64,\".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), \" */\");\n  } // For old IE\n\n  /* istanbul ignore if  */\n\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = css;\n  } else {\n    while (style.firstChild) {\n      style.removeChild(style.firstChild);\n    }\n\n    style.appendChild(document.createTextNode(css));\n  }\n}\n\nvar singleton = null;\nvar singletonCounter = 0;\n\nfunction addStyle(obj, options) {\n  var style;\n  var update;\n  var remove;\n\n  if (options.singleton) {\n    var styleIndex = singletonCounter++;\n    style = singleton || (singleton = insertStyleElement(options));\n    update = applyToSingletonTag.bind(null, style, styleIndex, false);\n    remove = applyToSingletonTag.bind(null, style, styleIndex, true);\n  } else {\n    style = insertStyleElement(options);\n    update = applyToTag.bind(null, style, options);\n\n    remove = function remove() {\n      removeStyleElement(style);\n    };\n  }\n\n  update(obj);\n  return function updateStyle(newObj) {\n    if (newObj) {\n      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {\n        return;\n      }\n\n      update(obj = newObj);\n    } else {\n      remove();\n    }\n  };\n}\n\nmodule.exports = function (list, options) {\n  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>\n  // tags it will allow on a page\n\n  if (!options.singleton && typeof options.singleton !== 'boolean') {\n    options.singleton = isOldIE();\n  }\n\n  list = list || [];\n  var lastIdentifiers = modulesToDom(list, options);\n  return function update(newList) {\n    newList = newList || [];\n\n    if (Object.prototype.toString.call(newList) !== '[object Array]') {\n      return;\n    }\n\n    for (var i = 0; i < lastIdentifiers.length; i++) {\n      var identifier = lastIdentifiers[i];\n      var index = getIndexByIdentifier(identifier);\n      stylesInDom[index].references--;\n    }\n\n    var newLastIdentifiers = modulesToDom(newList, options);\n\n    for (var _i = 0; _i < lastIdentifiers.length; _i++) {\n      var _identifier = lastIdentifiers[_i];\n\n      var _index = getIndexByIdentifier(_identifier);\n\n      if (stylesInDom[_index].references === 0) {\n        stylesInDom[_index].updater();\n\n        stylesInDom.splice(_index, 1);\n      }\n    }\n\n    lastIdentifiers = newLastIdentifiers;\n  };\n};\n\n//# sourceURL=webpack:///./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _assets_stylesheets_global_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../assets/stylesheets/global.css */ \"./assets/stylesheets/global.css\");\n/* harmony import */ var _assets_stylesheets_global_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_stylesheets_global_css__WEBPACK_IMPORTED_MODULE_0__);\n//Importation of CSS File\n\n//Importation of JS Files\n__webpack_require__(/*! ../assets/js/global.js */ \"./assets/js/global.js\")\n__webpack_require__(/*! ../assets/js/btninteraction.js */ \"./assets/js/btninteraction.js\")\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });