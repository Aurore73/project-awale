Projet Awalé
============

Le projet se déroulera sur 2 semaines par binôme.


# Objectifs

* Découverte de l'algorithmie
* Découverte de Javascript
* Manipulation du DOM
* Gestion d'événements Javascript

Compétences :
* Compétence 1 : maquettage
* Compétence 2 : web statique
* Compétence 3 : web dynamique

# Tâches

## Tâches demandées
* Réalisation d'une wireframe
* Développer un jeu awalé en JS (tour par tour pour 2 joueurs)

## Bonus

* Scoring
* IA pour jouer solo

# Organisation

## Etape 1
Les binômes définiront leur projet :
* maquettage
* organisation
* description de leur projet étape par étape

## Etape 2
Réalisation du jeu avec comme objectif de faire les tâches demandées.

## Etape 3

Une fois les tâches demandées réalisées, définir des tâches optionnelles et les réaliser.

### Organisation
1 master
1 branche dev
1 branche par personne

Merge vers dev quand une fonction est terminée.
Merge vers le master ensemble quand tout est fini.
Wireframe à réaliser pour la structure HTML.

Commit, commentaires... => en anglais.

Répartir les rôles : 
1) définir qui commence (Rachid)
2) prendre les graines (Rachid)
3) répartir les graines (Aurore)
4) gagner les graines
5) éléments spécifiques : sauter le trou de départ si >12
6) éléments spécifiques : prévoir un coup obligatoire si le joueur adverse n'a plus de graine
7) bouton de fin
8) proposer une nouvelle partie
9) bonus : scoring

Wireframe whimsical à finaliser (Aurore)

### A FAIRE
A prévoir en fin : responsive, design.


### Intéractions
1) Au départ on clique sur un bouton qui permet de choisir le joueur 1 ou 2. 
Une fois le joueur choisit, la partie du jeu le concernant est colorée en bleu ou vert et l'autre partie incliquable. 

2) Pour débuter le tour du joueur choisit, cliquer sur une case du côté du joueur. 

3) Automatiquement, les graines sont enlevées de la case, puis distribuées dans les cases à côté (dans le sens inverse des aiguilles du montre).
Si la distribution finit côté joueur actuel, le tour s'arrête.
Si la distribution finit côté joueur adverse, les graines sont retiréees du jeu.
A la fin quand le tour est terminée, un message indique que le tour est terminé et les cases du joueur suivant sont colorées et celles du précédent joueur sont grisées et incliquables.

4) Au final, quand il n'y a plus de graine du côté d'un joueur (ex J1), il faut obliger l'autre joueur (J2) à jouer le coup où une graine finit de l'autre côté (côté J1).

5) Si réalisable, la partie se terminera lorsque un des deux joueurs n'a plus de graine de son côté et que l'autre ne peut plus en mettre.
Sinon un bouton "fin" sera prévu pour terminer la partie bloquée.

### Règles de jeux
Règle 1 : But du jeu Le but du jeu est de s'emparer d'un maximum de graines. Le joueur qui a le plus de graines à la fin de la partie l'emporte.

Règle 2 : Le terrain de jeu Le terrain de jeu est divisé en deux territoires de 6 trous chacun. Votre territoire est en bas, c'est le territoire Sud; celui de votre adversaire est au dessus, c'est le territoire Nord. Au départ dans les douze trous sont réparties 48 graines (4 par trou).

Règle 3 : Le tour de jeu. Chaque joueur joue à son tour, celui qui joue en premier est tiré au hasard. Le joueur va prendre l'ensemble des graines présentes dans l'un des trous de son territoire et les distribuer, une par trou, dans le sens inverse des aiguilles d'une montre.

Règle 4 : Capture Si la dernière graine semée tombe dans un trou de l'adversaire comportant déjà 1 ou 2 graines, le joueur capture les 2 ou 3 graines résultantes. Les graines capturées sont sorties du jeu. (Le trou est alors laissé vide)

Règle 5: Capture multiple Lorsqu'un joueur s'empare de deux ou trois graines, si la case précédente contient également deux ou trois graines, elle sont capturées aussi, et ainsi de suite.

Règle 6: Bouclage Si le nombre de graines prises dans le trou de départ est supérieur à 11, cela fait que l'on va boucler un tour : auquel cas, à chaque passage, la case de départ est sautée et donc toujours laissée vide. Un trou contenant assez de graines pour faire une boucle complète s'appelle un Krou.

Règle 7: Donner à manger On n'a pas le droit d'affamer l'adversaire : De même, un joueur n'a pas le droit de jouer un coup qui prenne toutes les graines du camp de l'adversaire.

Règle 8: Fin de jeu Le jeu se termine lorsque : - Un joueur n'a plus de graines dans son camp et ne peut donc plus jouer L'adversaire capture alors les graines restantes.

- La partie boucle indéfiniment (la même situation se reproduit après un certain nombre de coups) Personne ne capture les graines restantes - L'un des joueurs abandonne L'adversaire capture les graines restantes.
